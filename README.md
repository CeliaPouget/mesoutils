
# <img src="man/figures/hex-mesoutils.png" align="right" alt="" width="120" />

<!-- README.md is generated from README.Rmd. Please edit that file -->

# mesoutils

<!-- badges: start -->
<!-- badges: end -->

The goal of mesoutils is to have a list of tools for daily work with R.

## Installation

You can install the development version of mesoutils like so:

``` r
# install.packages("mesoutils")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(mesoutils)
get_info_data(data = iris)
#> $dimension
#> [1] 150   5
#> 
#> $names
#> [1] "Sepal.Length" "Sepal.Width"  "Petal.Length" "Petal.Width"  "Species"
```

``` r
get_mean_data(data = iris)
#>   Sepal.Length Sepal.Width Petal.Length Petal.Width
#> 1     5.843333    3.057333        3.758    1.199333
```
